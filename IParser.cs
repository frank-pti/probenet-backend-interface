/*
 * Interfaces for ProbeNet Backends
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Base;

namespace ProbeNet.Backend
{
	/// <summary>
	/// Send sequence series metadata delegate.
	/// </summary>
	/// <param name="metadata">The metadata</param>
    public delegate void SendSequenceSeriesMetadataDelegate(SequenceSeriesMetadata metadata);
	/// <summary>
	/// Send sequence delegate.
	/// </summary>
	/// <param name="sequence">The sequence</param>
	public delegate void SendSequenceDelegate(Sequence sequence);
	/// <summary>
	/// Send data to source delegate.
	/// </summary>
	/// <param name="data">The data to be sent</param>
	/// <param name="offset">The index to start at</param>
	/// <param name="count">The amount of data to be sent</param>
	public delegate void SendDataToSourceDelegate(byte[] data, int offset, int count);
	/// <summary>
	/// Parsing error delegate.
	/// </summary>
	/// <param name="showInteractive">show error in user interface</param>
    /// <param name="cause">the exception that caused the error; set to null if no exception is involved</param>
	/// <param name="englishMessage">the message in english language</param>
	/// <param name="localizedMessage">localized message</param>
	public delegate void ParsingErrorDelegate(bool showInteractive, Exception e, string englishMessage, string localizedMessage);

    /// <summary>
    /// Parser for interpreting the incoming data from the device.
    /// </summary>
	public interface IParser
	{
        /// <summary>
        /// Occurs when sequence series metadata is completley parsed.
        /// </summary>
        event SendSequenceSeriesMetadataDelegate SendSequenceSeriesMetadata;

        /// <summary>
        /// Occurs when sequence is completly parsed.
        /// </summary>
		event SendSequenceDelegate SendSequence;

        /// <summary>
        /// Occurs when data should be sent to the datasource.
        /// </summary>
		event SendDataToSourceDelegate SendDataToSource;

        /// <summary>
        /// Occurs when parsing error occurs.
        /// </summary>
		event ParsingErrorDelegate ParsingError;

        /// <summary>
        /// Parse the specified data, beginning at index offset and count bytes length.
        /// </summary>
        /// <param name='data'>
        /// The data to parse.
        /// </param>
        /// <param name='offset'>
        /// The index to begin parsing the data.
        /// </param>
        /// <param name='count'>
        /// The amount of data to be parsed.
        /// </param>
		void Parse(byte[] data, int offset, int count);

        /// <summary>
        /// Reset the parser.
        /// </summary>
		void Reset();
	}
}
