/*
 * Interfaces for ProbeNet Backends
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using ProbeNet.Messages.Interface;

namespace ProbeNet.Backend
{
    /// <summary>
    /// Device handle.
    /// </summary>
    /// <remarks>Base class for all device handles.</remarks>
	public abstract class DeviceHandle
	{
        /// <summary>
        /// Gets the description.
        /// </summary>
        /// <value>
        /// The description of the device
        /// </value>
		public abstract IDeviceDescription Description
		{
			get;
		}

        /// <summary>
        /// Gets the parser.
        /// </summary>
        /// <value>
        /// The parser for incoming data from the device.
        /// </value>
		public abstract IParser Parser
		{
			get;
		}

        /// <summary>
        /// Gets a value indicating whether this <see cref="ProbeNet.Backend.DeviceHandle"/> requires license.
        /// </summary>
        /// <value>
        /// <c>true</c> if requires license; otherwise, <c>false</c>.
        /// </value>
        public abstract bool RequiresLicense
        {
            get;
        }
	}
}
