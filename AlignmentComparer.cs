/*
 * Interfaces for ProbeNet Backends
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.Collections;
#if !MICRO_FRAMEWORK
using System.Collections.Generic;
#endif
using ProbeNet.Messages.Interface;

namespace ProbeNet.Backend
{
    /// <summary>
    /// Compare the equality of two <see cref="IAlignment"/> objects.
    /// </summary>
#if MICRO_FRAMEWORK
    public class AlignmentComparer : IEqualityComparer
#else
    public class AlignmentComparer : EqualityComparer<IAlignment>, IEqualityComparer
#endif
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AlignmentComparer"/> class.
        /// </summary>
        public AlignmentComparer()
        {
        }

        #region implemented abstract members of System.Collections.Generic.EqualityComparer[Alignment]
        /// <returns>
        /// The hash code
        /// </returns>
        /// <since version='.NET 2.0'></since>
        /// <summary>
        /// Gets the hash code.
        /// </summary>
        /// <param name='alignment'>
        /// The alignment for which to get a hash code.
        /// </param>
#if MICRO_FRAMEWORK
        public int GetHashCode(IAlignment alignment)
#else
        public override int GetHashCode(IAlignment alignment)
#endif
        {
            int sideCode = 0;
#if MICRO_FRAMEWORK
            if (!alignment.SideValid)
#else
            if (alignment.Side == null)
#endif
 {
                sideCode = 1 << 7;
            } else {
#if MICRO_FRAMEWORK
                sideCode = (int)alignment.Side << 4;
#else
                sideCode = (int)alignment.Side.Value << 4;
#endif
            }
            int directionCode;
#if MICRO_FRAMEWORK
            if (!alignment.DirectionValid)
#else
            if (alignment.Direction == null)
#endif
 {
                directionCode = 1 << 3;
            } else {
#if MICRO_FRAMEWORK
                directionCode = alignment.Direction;
#else
                directionCode = alignment.Direction.Value;
#endif
            }
            return sideCode + directionCode;
        }

        /// <summary>
        /// Determines whether two alignments are equal.
        /// </summary>
        /// <param name='a'>
        /// The first alignment to compare.
        /// </param>
        /// <param name='b'>
        /// The second alignment to compare.
        /// </param>
        /// <returns><c>true</c> if the specified alignments are equal; otherwise, <c>false</c></returns>
#if MICRO_FRAMEWORK
        public bool Equals(IAlignment a, IAlignment b)
#else
        public override bool Equals(IAlignment a, IAlignment b)
#endif
        {
            return a.Equals(b);
        }
        #endregion

        /// <summary>
        /// Determines whether two alignments are equal.
        /// </summary>
        /// <param name='x'>
        /// The first object to compare.
        /// </param>
        /// <param name='y'>
        /// The second object to compare.
        /// </param>
        /// <returns><c>true</c> if the specified alignments are equal; otherwise, <c>false</c></returns>
        bool IEqualityComparer.Equals(object x, object y)
        {
            if ((x is IAlignment) && (y is IAlignment)) {
                return Equals(x as IAlignment, y as IAlignment);
            }
            return x.Equals(y);
        }

        /// <returns>
        /// The hash code
        /// </returns>
        /// <since version='.NET 2.0'></since>
        /// <summary>
        /// Gets the hash code.
        /// </summary>
        /// <param name='obj'>
        /// The alignment for which to get a hash code.
        /// </param>
        public int GetHashCode(object obj)
        {
            if (obj is IAlignment) {
                return GetHashCode((IAlignment)obj);
            }
            return obj.GetHashCode();
        }
    }
}

